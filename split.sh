#!/bin/bash

path=$1
file=$(basename "${path}")
device="${file%%.*}"
out_dir=$(dirname "${path}")

pushd $out_dir
rm -f $device-*.gz

config_off=$((`sfdisk -J $file | jq -r .partitiontable.partitions[1].start`))
config_siz=$((`sfdisk -J $file | jq -r .partitiontable.partitions[1].size`))

dd if=$file \
   of=$device-0.img \
	count=$config_off \
	bs=512
gzip -9 $device-0.img

dd if=$file \
   of=$device-1.img \
	conv=notrunc \
	bs=512 \
	skip=$config_off \
	count=$config_siz
gzip -9 $device-1.img

dd if=$file \
   of=$device-2.img \
	conv=notrunc \
	bs=512 \
	skip=$(($config_off + $config_siz))
gzip -9 $device-2.img

popd
